# Integer Matrix Inversion
This program performs matrix inversions without a working floating point unit 
utilizing software rational numbers, therefore the accuracy of the calculations might be questionable.
The results are compared against NumPy generated inverse matrices and the self test is failed if the 
numerator and denumerator don't match.

Set your matrix parameters in `gen_data.py` and run the Python script to generate the test data:
`python3 gen_data.py`
`gcc main.c -o test`
`./test`

You probably have to comment out A-Core specific header files to test this program on your local machine,
but it should run as if the A-Core test environment is setup correctly.
