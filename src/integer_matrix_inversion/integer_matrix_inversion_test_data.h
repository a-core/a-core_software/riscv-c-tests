// This file was generated with gen_data.py
#define NUM_MATRICES 2
#define N 3
#ifndef __MATRIX_INVERSION_TESTDATA_H
#define __MATRIX_INVERSION_TESTDATA_H
Rational matrix0[3][3] = {
	{{-10,1},{26,1},{-25,1}},
	{{12,1},{29,1},{1,1}},
	{{7,1},{4,1},{1,1}}
};
Rational matrix1[3][3] = {
	{{17,1},{11,1},{7,1}},
	{{-4,1},{-30,1},{27,1}},
	{{26,1},{25,1},{12,1}}
};
Rational inverse0[3][3] = {
	{{5,699},{-42,1165},{751,3495}},
	{{-1,699},{11,233},{-58,699}},
	{{-31,699},{74,1165},{-602,3495}}
};
Rational inverse1[3][3] = {
	{{207,917},{-43,4585},{-507,4585}},
	{{-150,917},{-22,4585},{487,4585}},
	{{-136,917},{139,4585},{466,4585}}
};
Rational (*matrices[NUM_MATRICES])[N][N] = { &matrix0, &matrix1 };
Rational (*inverses[NUM_MATRICES])[N][N] = { &inverse0, &inverse1 };
#endif // __MATRIX_INVERSION_TESTDATA_H
