#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "a-core-utils.h"
#include "a-core-csr.h"
#include "a-core.h"
#include "acore-gpio.h"
#include "acore-uart.h"

/* 
This program performs matrix inversions without a working floating point unit 
utilizing software rational numbers, therefore the accuracy of the calculations might be questionable.
The results are compared against NumPy generated inverse matrices and the self test is failed if the 
numerator and denumerator don't match.

Set your matrix parameters in gen_data.py and run the Python script to generate the test data:
python3 gen_data.py
gcc main.c -o test
./test

You probably have to comment out A-Core specific header files to test this program on your local machine,
but it should run as if the A-Core test environment is setup correctly.
*/

// Fake float 
typedef struct {
    int num;
    int den;
} Rational;

#include "integer_matrix_inversion_test_data.h"

// Find greatest common divisor of two numbers
int gcd(int a, int b) {
    a = abs(a);
    b = abs(b);
    while (b) {
        int temp = b;
        b = a % b;
        a = temp;
    }
    return a;
}

Rational simplify(Rational r) {
    int g = gcd(r.num, r.den);
    Rational result = {r.num / g, r.den / g};
    if (result.den < 0) {
        result.num = -result.num;
        result.den = -result.den;
    }
    return result;
}

Rational make_rational(int num, int den) {
    Rational r = {num, den};
    return simplify(r);
}

Rational multiply_rational(Rational a, Rational b) {
    Rational r = {a.num * b.num, a.den * b.den};
    return simplify(r);
}

Rational divide_rational(Rational a, Rational b) {
    Rational r = {a.num * b.den, a.den * b.num};
    return simplify(r);
}

Rational subtract_rational(Rational a, Rational b) {
    Rational r = {a.num * b.den - b.num * a.den, a.den * b.den};
    return simplify(r);
}

void print_matrix(Rational matrix[N][N]) {
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            if (matrix[i][j].den == 1)
                printf("%d\t", matrix[i][j].num);
            else
                printf("%d/%d\t", matrix[i][j].num, matrix[i][j].den);
        }
        printf("\n");
    }
}

void inverse_matrix(Rational matrix[N][N]) {
    // Original matrix augmented with identity matrix
    Rational augmented[N][2*N];
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            augmented[i][j] = matrix[i][j];
            augmented[i][j + N] = make_rational((i == j) ? 1 : 0, 1);
        }
    }

    // Gauss-Jordan elimination
    for (int i = 0; i < N; i++) {
        // Leading entry of the row
        Rational pivot = augmented[i][i];
        // Divide row by pivot
        for (int j = 0; j < 2*N; j++) {
            augmented[i][j] = divide_rational(augmented[i][j], pivot);
        }

        // Eliminate column
        for (int k = 0; k < N; k++) {
            if (k != i) {
                Rational factor = augmented[k][i];
                for (int j = 0; j < 2*N; j++) {
                    Rational prod = multiply_rational(factor, augmented[i][j]);
                    augmented[k][j] = subtract_rational(augmented[k][j], prod);
                }
            }

        }
    }

    // Copy back to original matrix
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            matrix[i][j] = augmented[i][j+N];
        }
    }
    
}

void main() {
    // Clear HPM [3,4,5,6] inhibits
    csr_clear_bits(CSR_MCOUNTINHIBIT, 0x78);

    // Invert input matrices
    for (int i = 0; i < NUM_MATRICES; i++) {
        inverse_matrix(*matrices[i]);
    }

    // Compare to NumPy generated inverse matrices
    for (int i = 0; i < NUM_MATRICES; i++) {
        for (int row = 0; row < N; row++) {
            for (int col = 0; col < N; col++) {
                if ((*matrices[i])[row][col].num != (*inverses[i])[row][col].num || (*matrices[i])[row][col].den != (*inverses[i])[row][col].den) {
                    test_fail();
                }
            }
        }
    }
    test_pass();
    test_end();
}
