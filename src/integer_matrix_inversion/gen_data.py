import numpy as np
from fractions import Fraction

def to_rational(x, max_denominator=2**32 - 1):
    return Fraction(float(x)).limit_denominator(max_denominator)

# Square matrix dimension
n = 3

# Number of matrices
n_matrices = 2

matrices = np.empty(n_matrices, dtype=object)
inverses = np.empty(n_matrices, dtype=object)

to_rational_vectorized = np.vectorize(to_rational)
for i in range(n_matrices):
    # Min/max values are limited because the size of denominator explodes really quickly with high values
    m = np.random.randint(low=(-2**5), high=(2**5), size=(n,n), dtype=np.int32)
    # Generate new matrix if/while old one is not invertible 
    while not (np.linalg.cond(m) < (1 / np.finfo(float).eps)):
        m = np.random.randint(low=(-2**31), high=(2**31-1), size=(n,n), dtype=np.int32)
    matrices[i] = m
    inverses[i] = to_rational_vectorized(np.linalg.inv(m))

# Parse Python matrices to C struct matrices 
def rational_matrix_to_header(file, matrix, name):
    rows = matrix.shape[0]
    cols = matrix.shape[1]
    if rows != cols:
        raise ValueError("Generated matrix is not a square matrix!")
    f.write(f"Rational {name}[{rows}][{cols}] = {{\n")
    for row_idx, row in enumerate(matrix):
        f.write(f"\t{{")
        for col_idx, col in enumerate(row):
            if col_idx == len(row) - 1:
                f.write(f"{{{col},1}}")
            else:
                f.write(f"{{{col},1}},")
        if row_idx == len(matrix) - 1:
            f.write(f"}}\n")
        else:
            f.write(f"}},\n")
    f.write(f"}};\n")

def fractional_matrix_to_header(file, matrix, name):
    rows = matrix.shape[0]
    cols = matrix.shape[1]
    if rows != cols:
        raise ValueError("Generated matrix is not a square matrix!")
    f.write(f"Rational {name}[{rows}][{cols}] = {{\n")
    for row_idx, row in enumerate(matrix):
        f.write(f"\t{{")
        for col_idx, col in enumerate(row):
            if col_idx == len(row) - 1:
                f.write(f"{{{col.numerator},{col.denominator}}}")
            else:
                f.write(f"{{{col.numerator},{col.denominator}}},")
        if row_idx == len(matrix) - 1:
            f.write(f"}}\n")
        else:
            f.write(f"}},\n")
    f.write(f"}};\n")

# Write header file
with open('integer_matrix_inversion_test_data.h', 'w') as f:
    f.write("// This file was generated with gen_data.py\n")
    f.write(f"#define NUM_MATRICES {n_matrices}\n")
    f.write(f"#define N {n}\n")
    f.write("#ifndef __MATRIX_INVERSION_TESTDATA_H\n")
    f.write("#define __MATRIX_INVERSION_TESTDATA_H\n")
    matrices_str = "{ "
    inverses_str = "{ "
    for idx, matrix in enumerate(matrices):
        name = f"matrix{idx}"
        matrices_str += "&"
        matrices_str += name
        if idx == len(matrices) - 1:
            matrices_str += " "
        else:
            matrices_str += ", "
        rational_matrix_to_header(f, matrix, name)
    for idx, inverse in enumerate(inverses):
        name = f"inverse{idx}"
        inverses_str += "&"
        inverses_str += name
        if idx == len(inverses) - 1:
            inverses_str += " "
        else:
            inverses_str += ", "
        fractional_matrix_to_header(f, inverse, name)
    matrices_str += "};"
    inverses_str += "};"
    f.write(f"Rational (*matrices[NUM_MATRICES])[N][N] = {matrices_str}\n")
    f.write(f"Rational (*inverses[NUM_MATRICES])[N][N] = {inverses_str}\n")
    f.write("#endif // __MATRIX_INVERSION_TESTDATA_H\n")
