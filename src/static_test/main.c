#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "a-core.h"
#include "a-core-utils.h"

// statically initialize some data in .data section
int global_a = 3;
int global_b = -15;

int fun()
{
  static int count_c = 0;
  count_c++;
  return count_c;
}

void main() {
    // init uart
    init_uart((volatile uint32_t*)A_CORE_AXI4LUART, BAUDRATE);

    int local_d = -5;
    int result = global_a + global_b 
        + local_d + fun() + fun();

    printf("%d + %d + %d + 1 + 2 = %d\n", global_a, global_b, local_d, result);

    // a0 stores test result
    if (result == -14) {
        test_pass();
    }
    else {
        test_fail();
    }

    // Writes 1 to mstopsim CSR to indicate sim stop
    test_end();

    // infinite loop
    for(;;);
}
