#include <stdint.h>
#include <math.h>
#include <a-core-utils.h>
#include <a-core.h>
#include <acore-gpio.h>

#define GPIO_IN  (volatile uint32_t*)(A_CORE_AXI4LGPIO+GPIO_IN_REG)
#define GPIO_OUT (volatile uint32_t*)(A_CORE_AXI4LGPIO+GPIO_OUT_REG)

void main() {

    // turn all outputs off
    gpo_init((volatile uint32_t*)GPIO_OUT);

    float A = 1.0;
    float B = 0.0;

    gpo_set_bit(GPIO_OUT, (int)A, 0);
    gpo_set_bit(GPIO_OUT, (int)B, 0);

    // SUM
    gpo_set_bit(GPIO_OUT, (int)(A + B), 1);
    gpo_set_bit(GPIO_OUT, (int)(B + B), 1);
 
    // SUB
    gpo_set_bit(GPIO_OUT, (int)(A - B), 2);
    gpo_set_bit(GPIO_OUT, (int)(B - B), 2);

    // MUL
    gpo_set_bit(GPIO_OUT, (int)(A * A), 3);
    gpo_set_bit(GPIO_OUT, (int)(A * B), 3);

    // DIV
    gpo_set_bit(GPIO_OUT, (int)(A / A), 4);
    gpo_set_bit(GPIO_OUT, (int)(B / A), 4);

    // FLQ
    gpo_set_bit(GPIO_OUT, (int)(A == A), 5);
    gpo_set_bit(GPIO_OUT, (int)(B == A), 5);

    // FLT
    gpo_set_bit(GPIO_OUT, (int)(A >= B), 6);
    gpo_set_bit(GPIO_OUT, (int)(A >= A), 6);

    // FLE
    gpo_set_bit(GPIO_OUT, (int)(A > A), 7);
    gpo_set_bit(GPIO_OUT, (int)(B > A), 7);

    // CVT
    int Ai = (int)A;
    int Bi = (int)B;
    gpo_set_bit(GPIO_OUT, Ai, 8);
    gpo_set_bit(GPIO_OUT, Bi, 8);

    // SQRT
    gpo_set_bit(GPIO_OUT, (int)sqrt(B), 9);
    gpo_set_bit(GPIO_OUT, (int)sqrt(A), 9);

    // MIN
    gpo_set_bit(GPIO_OUT, (int)fmin(A,B), 10);
    gpo_set_bit(GPIO_OUT, (int)fmin(A,A), 10);
    
    // MAX
    gpo_set_bit(GPIO_OUT, (int)fmax(B,B), 11);
    gpo_set_bit(GPIO_OUT, (int)fmax(A,B), 11);
}
