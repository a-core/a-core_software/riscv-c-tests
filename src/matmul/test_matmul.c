#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include "a-core.h"
#include "a-core-utils.h"
#include "a-core-csr.h"
#include "matmul_testdata.h"

// statically allocate buffer for result matrix C
int32_t C[CSIZE];

// computes matrix multiplication AB = C
// unoptimized reference implementation
//
// Parameters:
//     ptrA = Pointer to input matrix A.
//     ptrB = Pointer to input matrix B.
//     ptrC = Pointer to output matric C.
//     hA   = Height of input matrix A (number of rows).
//     wA   = Width of input matrix A (number of columns).
//     wB   = Width of input matrix B (number of rows).
void matmul(
    const int32_t * restrict ptrA,
    const int32_t * restrict ptrB,
    int32_t * restrict ptrC,
    uint32_t hA,
    uint32_t wA,
    uint32_t wB
) {
    for (int i = 0; i < hA; i++) {
        for (int j = 0; j < wB; j++) {
            int32_t acc = 0;
            for (int k = 0; k < wA; k++) {
                int32_t Aik = ptrA[i*wA + k];
                int32_t Bkj = ptrB[k*wB + j];
                acc += Aik * Bkj;
            }
            ptrC[i*wB + j] = acc;
        }
    }
}

// computes matrix multiplication AB = C
// column loop unrolled by eight iterations (wB must be a multiple of eight)
//
// Parameters:
//     ptrA = Pointer to input matrix A.
//     ptrB = Pointer to input matrix B.
//     ptrC = Pointer to output matric C.
//     hA   = Height of input matrix A (number of rows).
//     wA   = Width of input matrix A (number of columns).
//     wB   = Width of input matrix B (number of rows).
void matmul_uc8(
    const int32_t * restrict ptrA,
    const int32_t * restrict ptrB,
    int32_t * restrict ptrC,
    uint32_t hA,
    uint32_t wA,
    uint32_t wB
) {
    for (int i = 0; i < hA; i++) {
        for (int j = 0; j < wB; j+=8) {
            int32_t acc0 = 0;
            int32_t acc1 = 0;
            int32_t acc2 = 0;
            int32_t acc3 = 0;
            int32_t acc4 = 0;
            int32_t acc5 = 0;
            int32_t acc6 = 0;
            int32_t acc7 = 0;
            for (int k = 0; k < wA; k++) {
                int32_t Aik = ptrA[i*wA + k];
                int32_t Bkj0 = ptrB[k*wB + j + 0];
                int32_t Bkj1 = ptrB[k*wB + j + 1];
                int32_t Bkj2 = ptrB[k*wB + j + 2];
                int32_t Bkj3 = ptrB[k*wB + j + 3];
                int32_t Bkj4 = ptrB[k*wB + j + 4];
                int32_t Bkj5 = ptrB[k*wB + j + 5];
                int32_t Bkj6 = ptrB[k*wB + j + 6];
                int32_t Bkj7 = ptrB[k*wB + j + 7];
                acc0 += Aik * Bkj0;
                acc1 += Aik * Bkj1;
                acc2 += Aik * Bkj2;
                acc3 += Aik * Bkj3;
                acc4 += Aik * Bkj4;
                acc5 += Aik * Bkj5;
                acc6 += Aik * Bkj6;
                acc7 += Aik * Bkj7;
            }
            ptrC[i*wB + j + 0] = acc0;
            ptrC[i*wB + j + 1] = acc1;
            ptrC[i*wB + j + 2] = acc2;
            ptrC[i*wB + j + 3] = acc3;
            ptrC[i*wB + j + 4] = acc4;
            ptrC[i*wB + j + 5] = acc5;
            ptrC[i*wB + j + 6] = acc6;
            ptrC[i*wB + j + 7] = acc7;
        }
    }
}

// computes matrix multiplication AB = C
// row loop unrolled by four iterations (hA must be a multiple of four)
// column loop unrolled by four iterations (wB must be a multiple of four)
//
// Parameters:
//     ptrA = Pointer to input matrix A.
//     ptrB = Pointer to input matrix B.
//     ptrC = Pointer to output matric C.
//     hA   = Height of input matrix A (number of rows).
//     wA   = Width of input matrix A (number of columns).
//     wB   = Width of input matrix B (number of rows).
void matmul_ur4_uc4(
    const int32_t * restrict ptrA,
    const int32_t * restrict ptrB,
    int32_t * restrict ptrC,
    uint32_t hA,
    uint32_t wA,
    uint32_t wB
) {
    for (int i = 0; i < hA; i+=4) {
        for (int j = 0; j < wB; j+=4) {
            int32_t acc00 = 0;
            int32_t acc01 = 0;
            int32_t acc02 = 0;
            int32_t acc03 = 0;
            int32_t acc10 = 0;
            int32_t acc11 = 0;
            int32_t acc12 = 0;
            int32_t acc13 = 0;
            int32_t acc20 = 0;
            int32_t acc21 = 0;
            int32_t acc22 = 0;
            int32_t acc23 = 0;
            int32_t acc30 = 0;
            int32_t acc31 = 0;
            int32_t acc32 = 0;
            int32_t acc33 = 0;

            for (int k = 0; k < wA; k++) {
                int32_t Aik0 = ptrA[(i+0)*wA + k];
                int32_t Aik1 = ptrA[(i+1)*wA + k];
                int32_t Aik2 = ptrA[(i+2)*wA + k];
                int32_t Aik3 = ptrA[(i+3)*wA + k];
                int32_t Bkj0 = ptrB[k*wB + j + 0];
                int32_t Bkj1 = ptrB[k*wB + j + 1];
                int32_t Bkj2 = ptrB[k*wB + j + 2];
                int32_t Bkj3 = ptrB[k*wB + j + 3];

                acc00 += Aik0 * Bkj0;
                acc01 += Aik0 * Bkj1;
                acc02 += Aik0 * Bkj2;
                acc03 += Aik0 * Bkj3;
                acc10 += Aik1 * Bkj0;
                acc11 += Aik1 * Bkj1;
                acc12 += Aik1 * Bkj2;
                acc13 += Aik1 * Bkj3;
                acc20 += Aik2 * Bkj0;
                acc21 += Aik2 * Bkj1;
                acc22 += Aik2 * Bkj2;
                acc23 += Aik2 * Bkj3;
                acc30 += Aik3 * Bkj0;
                acc31 += Aik3 * Bkj1;
                acc32 += Aik3 * Bkj2;
                acc33 += Aik3 * Bkj3;

            }
            ptrC[(i+0)*wB + j + 0] = acc00;
            ptrC[(i+0)*wB + j + 1] = acc01;
            ptrC[(i+0)*wB + j + 2] = acc02;
            ptrC[(i+0)*wB + j + 3] = acc03;
            ptrC[(i+1)*wB + j + 0] = acc10;
            ptrC[(i+1)*wB + j + 1] = acc11;
            ptrC[(i+1)*wB + j + 2] = acc12;
            ptrC[(i+1)*wB + j + 3] = acc13;
            ptrC[(i+2)*wB + j + 0] = acc20;
            ptrC[(i+2)*wB + j + 1] = acc21;
            ptrC[(i+2)*wB + j + 2] = acc22;
            ptrC[(i+2)*wB + j + 3] = acc23;
            ptrC[(i+3)*wB + j + 0] = acc30;
            ptrC[(i+3)*wB + j + 1] = acc31;
            ptrC[(i+3)*wB + j + 2] = acc32;
            ptrC[(i+3)*wB + j + 3] = acc33;
        }
    }
}

void main() {
    // init peripherals
    init_uart((volatile uint32_t*)A_CORE_AXI4LUART, BAUDRATE);

    uint32_t start_mcycle = csr_read(CSR_MCYCLE);
    uint32_t start_mcycleh = csr_read(CSR_MCYCLEH);
    uint32_t start_minstret = csr_read(CSR_MINSTRET);

    // matmul(A, B, C, M, N, P);
    matmul_ur4_uc4(A, B, C, M, N, P);

    uint32_t end_mcycle = csr_read(CSR_MCYCLE);
    uint32_t end_mcycleh = csr_read(CSR_MCYCLEH);
    uint32_t end_minstret = csr_read(CSR_MINSTRET);

    // check if result is correct before outputting statistics
    for (int i = 0; i < CSIZE; i++) {
        if (C[i] != C_correct[i]) {
            test_fail();
        }
    }

    // compute elapsed number of cycles and retired instructions
    uint64_t start_cycles = ((uint64_t)start_mcycleh << 32) | start_mcycle;
    uint64_t end_cycles = ((uint64_t)end_mcycleh << 32) | end_mcycle;
    uint64_t elapsed_cycles = end_cycles - start_cycles;
    uint32_t elapsed_inst = end_minstret - start_minstret;

    // FIXME: uint64_t print with format specifier lld or PRIu64 is broken.
    //        print in hex in two uint32_t chunks as a workaround.
    printf("elapsed cycles = 0x%08lx%08lx\n", (uint32_t)(elapsed_cycles>>32), (uint32_t)elapsed_cycles);
    printf("elapsed instrs = %ld\n", elapsed_inst);

    test_pass();

    // Writes 1 to mstopsim CSR to indicate sim stop
    test_end();

    // infinite loop
    for(;;);
}
