import numpy as np

# Reference model for matrix multiply
#    A      B    =    C
# (M x N)(N x P) = (M x P)

# Test set size
M = 32
N = 32
P = 32
ASIZE = M*N
BSIZE = N*P
CSIZE = M*P

# M*P inner products, each of which requires N MAC operations.
# (N multiplications and N-1 additions, but we can consider Nth MAC as addition to zero).
num_mac = M*P*N
# print(num_mac)

rng = np.random.default_rng(seed=0)

# compute matrix multiply with pseudorandom numbers
# choose rand{min,max} so that matmul result C doesn't contain overflows
randmin = -2**12
randmax = 2**12
A = rng.integers(randmin, randmax, (M,N))
B = rng.integers(randmin, randmax, (N,P))
C = np.matmul(A,B)

# assert that we don't have int32_t {under,over}flows with chosen parameters
# python can represent arbitrary size integers, so check if they are out of
# the representable int32_t range.
int32_max = 2**31-1
int32_min = -2**31
assert (C <= int32_max).all(), "matmul result should not overflow int32_t"
assert (C >= int32_min).all(), "matmul result should not underflow int32_t"

def serialize_matrix_as_int32_t_array(fd, mat, name):
    num_elems = mat.size
    fd.write(f"const int32_t {name}[{num_elems}] = {{")
    flat_mat = mat.flatten()
    for i in range(num_elems-1):
        fd.write(f"{flat_mat[i]}, ")
    fd.write(f"{flat_mat[-1]}}};\n")

# write header
with open('matmul_testdata.h', 'w') as fd:
    fd.write("// This file was generated with gen_data.py\n")
    fd.write(f"#define M {M}\n")
    fd.write(f"#define N {N}\n")
    fd.write(f"#define P {P}\n")
    fd.write(f"#define ASIZE {ASIZE}\n")
    fd.write(f"#define BSIZE {BSIZE}\n")
    fd.write(f"#define CSIZE {CSIZE}\n")
    fd.write(f"// Number of required MAC operations = {num_mac}\n")
    fd.write("#ifndef __MATMUL_TESTDATA_H\n")
    fd.write("#define __MATMUL_TESTDATA_H\n")
    fd.write("#include <stdint.h>\n")
    serialize_matrix_as_int32_t_array(fd, A, "A")
    serialize_matrix_as_int32_t_array(fd, B, "B")
    serialize_matrix_as_int32_t_array(fd, C, "C_correct")
    fd.write("#endif // __MATMUL_TESTDATA_H\n")
