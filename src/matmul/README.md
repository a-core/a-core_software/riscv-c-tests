# Matrix Multiply

Computes matrix-matrix product AB = C, with int32_t data type.

`gen_data.py` can be used to generate test inputs and model results as a header file.

This test is self-checking and compares the computed results to those obtained with a python reference model.