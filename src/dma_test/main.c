#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include "a-core-utils.h"
#include "a-core.h"
#include "acore-gpio.h"

// New accessor functions
#include "a-core-dma.h"

uint32_t src_data[10] = {0xBEEFBABE,0xBAAAAAAD,0xCAFEBABE,0xC0FFEEEE,0xDEADBEEF,0xDADABABE,0xBAADBABE,0xF00DCAFE,0xDEADC0DE,0xDABDABDA};
uint32_t target_buffer[10];

// Goal of this test is to transfer the contents of
// src_data to target_buffer by using the DMA accessor methods
void main() {
    // Init UART
    volatile uint32_t* uart_base_addr = (volatile uint32_t*) A_CORE_AXI4LUART;
    init_uart(uart_base_addr, BAUDRATE);

    // Init DMA read
    set_dma_read(
        (uint32_t)A_CORE_AXI4LDMA,                                  // DMA address
        (uint32_t)src_data,                                         // Read start
        (uint32_t)src_data + sizeof(src_data) - sizeof(uint32_t),   // Read end
        (uint32_t)sizeof(uint32_t)                                  // Read increment
    );
    
    // Init DMA read
    set_dma_write(
        (uint32_t)A_CORE_AXI4LDMA,                                      // DMA address
        (uint32_t)target_buffer,                                        // Write start
        (uint32_t)target_buffer + sizeof(src_data) - sizeof(uint32_t),  // Write end
        (uint32_t)sizeof(uint32_t)                                      // Write increment
    );

    // Start DMA transfer, and don't loop
    start_dma(A_CORE_AXI4LDMA);

    // Wait until done-bit goes high
    wait_for_dma_done(A_CORE_AXI4LDMA);
    
    printf(" i |  src_data  | target_buffer\n");
    printf("-------------------------------\n");
    for (int i = 0; i < 10; i++) {
        // Check if target buffer value matches src_data
        printf("%2d | 0x%8lX | 0x%8lX\n", i, src_data[i], target_buffer[i]);
        if (target_buffer[i] != src_data[i]) {
            printf("Mismatch at index %d\n", i);
            test_fail();
        }
    }

    test_pass();
}
