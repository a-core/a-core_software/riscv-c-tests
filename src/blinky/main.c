#include <stdint.h>

#include "a-core-utils.h"
#include "a-core.h"
#include "acore-gpio.h"

void main() {
    // turn all outputs off
    volatile uint32_t* gpo_addr = (volatile uint32_t*) (A_CORE_AXI4LGPIO+GPIO_OUT_REG);
    gpo_init((volatile uint32_t*)gpo_addr);

    // shift asserted output around in a loop for a while
    uint32_t bits = 1;
    for (int i = 0; i < 64+1; i++) {
        gpo_write((volatile uint32_t*)gpo_addr, bits);
        if (bits == 0)
            bits = 1;
        else
            bits <<= 1;
    }
    gpo_write((volatile uint32_t*)gpo_addr, 0);

    // do a cool pattern
    for (int i = 0; i < 32; i++)
        gpo_set_bit(gpo_addr, 1, i);
    for (int i = 0; i < 32; i++)
        gpo_set_bit(gpo_addr, 0, i);

    // blink four LEDs with delay
    gpo_init((volatile uint32_t*)gpo_addr);
    int thresh = 100000;
    int i = 0;
    for (;;) {
        for (int i = 0; i < 4; i++) {
            gpo_set_bit(gpo_addr, 1, i);
            delay(thresh);
        }
        for (int i = 0; i < 4; i++) {
            gpo_set_bit(gpo_addr, 0, i);
            delay(thresh);
        }

    }
}
