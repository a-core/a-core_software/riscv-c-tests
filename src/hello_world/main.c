#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "a-core.h"
#include "a-core-utils.h"
#include "acore-uart.h"

// ANSI escape sequences for terminal colors
const char* BLUE    = "\x1b[34m";
const char* RED     = "\x1b[31m";
const char* YELLOW  = "\x1b[33m";
const char* RESET   = "\x1b[39m";

#ifdef sim
    #define DELAY 4
#else
    #define DELAY 1000000
#endif

void main() {
    // TODO: debug why newlib nano printf doesn'f work with floats
    // --specs=nano.specs -u _printf_float doesn't seem to help
    // The binary size is increased so there's something more being,
    // linked in. However with printf float support enabled printing
    // doesn't work at all (even with no formatting).
    // printf("Pi is %f\n", 3.14f);

    // init uart
    init_uart((volatile uint32_t*)A_CORE_AXI4LUART, BAUDRATE);

    /* test reading tx_byte */
    // send 'A'
    transmit_byte((volatile uint8_t*)A_CORE_AXI4LUART, 'A');

    // try reading `tx_byte` until transfer is complete
    wait_for_uart_ready((volatile uint8_t*)A_CORE_AXI4LUART);

    // print number with hex formatting
    printf("Hello, world! 0x%08x\n", 305419896);

    const int n_delay = DELAY;
    //const int n_delay = 0;

    // infinite loop
    for(;;) {
        // print number with hex formatting
        printf("Hello, world! 0x%08x\n", 305419896);
        delay(n_delay);

        // ANSI escape sequences work as well!
        printf("A%s?%s-Core\n", RED, RESET);
        delay(n_delay);
        printf("A%s\"%s-Core\n", YELLOW, RESET);
        delay(n_delay);
        printf("A%s!%s-Core\n", BLUE, RESET);
        delay(n_delay);
    }
}
