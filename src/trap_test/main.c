// statically initialize some data in .data section
int result = 3;

// compute factorial of an integer recursively
int factorial(int n) {
    if (n == 0)
        return 1;
    else
        return n*factorial(n-1);
}

void main() {
    // test function calls
    int a = 4;
    result = factorial(a);

    // test memory write
    *(volatile int*)(0x20000000) = result;

    // test memory read
    volatile int r = *(volatile int*)(0x20000000);

    asm("li t1, 0x20000000");
    asm("li t2, 0");
    asm("sb t2, 2(t1)");
    asm("addi t2, t2, 1");
    asm("sb t2, 3(t1)");
    asm("addi t2, t2, 1");
    asm("sh t2, 0(t1)");
    asm("addi t2, t2, 1");
    asm("sh t2, 1(t1)");
    asm("addi t2, t2, 1");
    asm("addi t2, t2, 1");

    // infinite loop
    for(;;);
}
