.section .init, "ax"
.global _start
_start:

    # Disable interrupts
    csrc mstatus, 8

    # init global pointer
    # temporarily disable linker relaxations as we can't
    # use gp relative addressing for setting gp itself
    .option push
    .option norelax
    la gp, __global_pointer$
    .option pop

    # Clear the bss segment
    la      a0, __bss_start
    la      a2, _end
    sub     a2, a2, a0
    li      a1, 0
    call    memset

    # Init stack and frame pointers
    la sp, __stack_top
    add s0, sp, zero

    # Initialize trap vector base address
    # This also leaves MODE-bit as 0, meaning direct mode (all traps go to the same handler)
    la t0, _isr_table
    csrw mtvec, t0
    
    # Enable interrupts
    csrs mstatus, 8

    jal zero, main


_base_isr:
    # Disable interrupts by clearing MIE (machine interrupt enable) bit
    csrci mstatus, 8

    # Save all registers to stack, except for zero
    addi sp, sp, -128
    sw x1,   1*4(sp)
    sw x2,   2*4(sp)
    sw x3,   3*4(sp)
    sw x4,   4*4(sp)
    sw x5,   5*4(sp)
    sw x6,   6*4(sp)
    sw x7,   7*4(sp)
    sw x8,   8*4(sp)
    sw x9,   9*4(sp)
    sw x10, 10*4(sp)
    sw x11, 11*4(sp)
    sw x12, 12*4(sp)
    sw x13, 13*4(sp)
    sw x14, 14*4(sp)
    sw x15, 15*4(sp)
    sw x16, 16*4(sp)
    sw x17, 17*4(sp)
    sw x18, 18*4(sp)
    sw x19, 19*4(sp)
    sw x20, 20*4(sp)
    sw x21, 21*4(sp)
    sw x22, 22*4(sp)
    sw x23, 23*4(sp)
    sw x24, 24*4(sp)
    sw x25, 25*4(sp)
    sw x26, 26*4(sp)
    sw x27, 27*4(sp)
    sw x28, 28*4(sp)
    sw x29, 29*4(sp)
    sw x30, 30*4(sp)
    sw x31, 31*4(sp)

    # Handle the trap

    csrr a0, mcause
    csrr a1, mepc

    jal handle_base_trap

    csrw mepc, a0

    # Load registers from stack
    lw x1,   1*4(sp)
    lw x2,   2*4(sp)
    lw x3,   3*4(sp)
    lw x4,   4*4(sp)
    lw x5,   5*4(sp)
    lw x6,   6*4(sp)
    lw x7,   7*4(sp)
    lw x8,   8*4(sp)
    lw x9,   9*4(sp)
    lw x10, 10*4(sp)
    lw x11, 11*4(sp)
    lw x12, 12*4(sp)
    lw x13, 13*4(sp)
    lw x14, 14*4(sp)
    lw x15, 15*4(sp)
    lw x16, 16*4(sp)
    lw x17, 17*4(sp)
    lw x18, 18*4(sp)
    lw x19, 19*4(sp)
    lw x20, 20*4(sp)
    lw x21, 21*4(sp)
    lw x22, 22*4(sp)
    lw x23, 23*4(sp)
    lw x24, 24*4(sp)
    lw x25, 25*4(sp)
    lw x26, 26*4(sp)
    lw x27, 27*4(sp)
    lw x28, 28*4(sp)
    lw x29, 29*4(sp)
    lw x30, 30*4(sp)
    lw x31, 31*4(sp)
    addi sp, sp, 128

    # Enable interrupts
    csrsi mstatus, 8

    # Return from trap handler
    mret

# Base trap handler
# This could also be written in C
handle_base_trap:
    addi a0, a1, 4 # Forward PC by 1 instruction
    ret

# Table for interrupts and exceptions
# Jump-instructions act as interrupt/exception vectors
_isr_table:
    jal zero, _base_isr