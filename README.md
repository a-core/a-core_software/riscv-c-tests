# RISC-V C Test Programs
This repository hosts C test programs for A-Core. To add a new program copy the files in 
the `riscv-c-template` to a new directory. The set of enabled hardware features can be
controlled with the `-march` and `-mabi` flags.

## Repository Structure
* README.md -- you are reading it right now
* `LICENSE_AALTO.txt` -- Software license.
* `init_submodules.sh` -- Helper script for recursively cloning git submodules.
* `riscv-c-template` -- A minimal template for compiling a C program for A-Core.

## Programs
* `blinky` -- Blinking an LED over memory mapped IO

## Compiling Programs
All programs can be compiled by executing:
```shell
$ ./configure && make
```
